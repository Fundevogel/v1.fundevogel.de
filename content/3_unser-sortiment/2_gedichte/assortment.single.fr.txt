Title: Poèmes

----

Text:

## De la poésie pour l'âme ...
> Hänsel und Knödel,
> die gingen in den Wald.
> Nach längerem Getrödel
> rief Hänsel plötzlich: „Halt!“-
>
> Ihr alle kennt die Fabel,
> des Schicksals dunklen Lauf:
> Der Hänsel nahm die Gabel
> und aß den Knödel auf.
>
> <footer>Michael Ende</footer>

----

Layouts: [{"attrs":{"style":"default","heading":""},"columns":[{"blocks":[{"content":{"level":"h2","text":"Voir le langage se d\u00e9velopper"},"id":"992d6bc0-fb0c-4288-bf23-7ed27567e059","isHidden":false,"type":"heading"},{"content":{"text":"<p>Les enfants aiment les r\u00e9p\u00e9titions et les rimes et s'amusent beaucoup \u00e0 jouer eux-m\u00eames avec la langue : tordre les lettres et les mots, former des rimes (absurdes) et cr\u00e9er des n\u00e9ologismes. De bons po\u00e8mes pour enfants qui font r\u00e9sonner la langue, \u00e9veillent le plaisir de la po\u00e9sie et stimulent l'imagination se trouvent par exemple dans le merveilleux recueil de po\u00e8mes \"Ich liebe dich wie Apfelmus\" d'Amelie Fried et Sybille Hein.<\/p>"},"id":"8ea60aa6-9566-42ef-8d11-65604402f658","isHidden":false,"type":"text"}],"id":"7e4852d1-e4ce-461f-ad73-597520269569","width":"1\/1"}],"id":"273f0028-a8a1-4a7b-b6f8-fd97679ad803"}]

----

Short: De nombreux poèmes pour enfants font vibrer les mots, briller les phrases et scintiller les idiomes ! Les enfants aiment les répétitions et les rimes et s'amusent beaucoup à jouer eux-mêmes avec le langage : tordre les lettres et les mots, former des rimes (absurdes) et créer des néologismes.

----

Meta-title: Gedichte & Lyrisches

----

Meta-description: Les enfants aiment les répétitions et les rimes et s'amusent à tordre les lettres et les mots, à former des rimes (absurdes) et à créer ainsi de nouvelles choses.

----

Meta-keywords: 

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image: 

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 

----

Og-type-article-modified-time: 

----

Og-type-article-expiration-time: 

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image: 

----

Twitter-card-type: summary_large_image

----

Twitter-site: 

----

Twitter-creator: 