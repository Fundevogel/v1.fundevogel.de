Title: Calendar

----

Slug: calendar

----

Text:

## We look forward to seeing you
Here you will find everything you need to know about our upcoming events.

It's also possible to subscribe to our event calendar - this way, all upcoming events will appear in your calendar or app automagically!

If you want to see what we have done in the past, you can also find the past events (link: kalender/vergangene-veranstaltungen text: here).

----

Archive:

## Good times
Here you will find all information about the past events of the Fundevogel.

If you want to see what we're currently up to, you can also find the upcoming events (link: kalender text: here).

----

Meta-title: Upcoming events

----

Meta-description: We organize all kinds of book-related events from readings and book tables for school classes to the Lirum-Larum reading festival - see you there!

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image:

- kalender.jpg

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 

----

Og-type-article-modified-time: 

----

Og-type-article-expiration-time: 

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image:

- kalender.jpg

----

Twitter-card-type: summary_large_image

----

Twitter-site: 

----

Twitter-creator: 

----

Meta-keywords: 

----

Date: 2017-02-10