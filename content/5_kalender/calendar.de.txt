Title: Kalender

----

Text:

## Wir freuen uns auf Euch
Hier findet Ihr alle Informationen zu den aktuellen Veranstaltungen des Fundevogels.

Unseren Veranstaltungskalender könnt Ihr mit einem Klick auf das nebenstehende Bild auch abonnieren - so finden künftig alle Veranstaltungen ihren Weg direkt in Euren Kalender! Alternativ auch als Feed in den Formaten (link: kalender/json text: JSON target: blank) und (link: kalender/rss text: RSS target: blank).

Wenn Ihr sehen wollt, was wir in der Vergangenheit so alles auf die Beine gestellt haben, dann schaut doch mal vorbei in unserem (link: kalender/vergangene-veranstaltungen text: Archiv).

----

Archive:

## Wir haben uns gefreut!
Hier findet ihr alle Informationen zu den vergangenen Veranstaltungen des Fundevogels.

Wenn ihr sehen wollt, welche aktuellen Veranstaltungen wir auf die Beine stellen, dann schaut doch mal vorbei in unserem (link: kalender text: Kalender).

----

Cover:

- kalender.jpg

----

Perpage-wanted: true

----

Perpage: 10

----

Meta-title: Veranstaltungskalender

----

Meta-description: Wir veranstalten von Lesungen und Büchertischen für Schulklassen bis zum Lirum-Larum-Lesefest alle möglichen Veranstaltungen rund um Bücher - seid dabei!

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image:

- kalender.jpg

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 

----

Og-type-article-modified-time: 

----

Og-type-article-expiration-time: 

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image:

- kalender.jpg

----

Twitter-card-type: summary_large_image

----

Twitter-site: 

----

Twitter-creator: 

----

Meta-keywords: 

----

Date: 2017-02-10