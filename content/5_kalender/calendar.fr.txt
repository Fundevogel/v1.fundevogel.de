Title: Agenda

----

Slug: agenda

----

Text:

## Venez nombreux!
Cliquez sur le lien pour pouvoir consulter notre agenda!

Il est également possible de s’abonner à notre calendrier des événements. De cette manière, tous les événements à venir apparaîtront automatiquement dans votre calendrier!

Vous pouvez également, si vous le souhaitez, jeter un oeil aux événements (link: kalender/vergangene-veranstaltungen text: passés), pour vous faire une petite idée de ce que nous proposons.

----

Archive:

## Nous étions heureux!
C'est avec joie que nous vous présentons ici toutes les informations sur les événements passés du Fundevogel.

Si vous souhaitez voir les événements que nous organisons actuellement, jetez un coup d'œil à notre (link: kalender text: calendrier).

----

Meta-title: Événements à venir

----

Meta-description: Nous organisons toutes sortes d'événements liés au livre, des lectures et des tables de lecture pour les classes scolaires au festival de lecture Lirum-Larum.

----

Meta-canonical-url: 

----

Meta-author: 

----

Meta-image: 

----

Meta-phone-number: 

----

Og-title: 

----

Og-description: 

----

Og-image:

- kalender.jpg

----

Og-site-name: 

----

Og-url: 

----

Og-audio: 

----

Og-video: 

----

Og-determiner: 

----

Og-type: website

----

Og-type-article-published-time: 

----

Og-type-article-modified-time: 

----

Og-type-article-expiration-time: 

----

Og-type-article-author: 

----

Og-type-article-section: 

----

Og-type-article-tag: 

----

Twitter-title: 

----

Twitter-description: 

----

Twitter-image:

- kalender.jpg

----

Twitter-card-type: summary_large_image

----

Twitter-site: 

----

Twitter-creator: 

----

Meta-keywords: 

----

Date: 2017-02-10