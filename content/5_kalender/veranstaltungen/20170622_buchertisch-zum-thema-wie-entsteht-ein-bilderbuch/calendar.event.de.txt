Title: Büchertisch zum Thema: "Wie entsteht ein Bilderbuch?"

----

Location: Museum Natur und Mensch, 2. OG, Gerberau 32, 79098 Freiburg

----

Date: 2017-06-22

----

Link:

----

Category: Büchertisch

----

Text:

Anlässlich der Sonderausstellung „Todsicher? Letzte Reise ungewiss“ veranschaulicht die Illustratorin und Autorin Kathrin Schärer die Entstehung eines Bilderbuchs anhand des Buches „Der Tod auf dem Apfelbaum“. Auf weitere Buchempfehlungen folgt ein offener Austausch zum Thema Tod im Kinderbuch. Diese Veranstaltung begleiten wir mit einem Büchertisch. Für ErzieherInnen, LehrerInnen und Interessierte.

*Eintritt 8 Euro inkl. Museumseintritt mit Anmeldung.
In Zusammenarbeit u.a. mit dem Literaturbüro Freiburg.*

----

Timestart: 16:30:00

----

Timeend: 18:00:00

----

Dateend: 2017-06-22

----

Showtime: true

----

Multipledays: false

----

Draft: 1
