Title: Café-lecture au Café Marcel

----

Location: Café Marcel (Stadtpark)

----

Date: 2017-03-29

----

Link:

----

Category: Lesung

----

Text:

Es geht weiter mit der zweiten Café-Lesung, diesmal für die Nachmittagspause! Wie bei der ersten Veranstaltung, gelesen wird auf französisch für Kinder von 3 bis 6.

Bitte prüft am Tag der Lesung auf unserer Webseite oder beim (link: http://www.cafemarcel.de/ text: Café Marcel), wo die Lesung stattfindet (bei Regen wird im Fundevogel vorgelesen).

*Eintritt frei, keine Reservierung notwendig.*

----

Timestart: 15:30:00

----

Timeend:

----

Dateend: 2017-03-29

----

Showtime: true

----

Multipledays: false

----

Draft: 1
