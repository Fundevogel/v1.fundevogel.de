Title: Afrika ist bunt - Lesung und Gespräch mit Nasrin Siege

----

Location: Katholische Akademie

----

Date: 2019-04-30

----

Link:

----

Category: Lesung

----

Text:

Am 30. April ist die Kinder- und Jugendbuchautorin Nasrin Siege zu Gast in der Katholischen Akademie in Freiburg. Im Rahmen der Reihe *"Kunst für Kinder"* wird die Autorin von ihren Erfahrungen mit dem bunten Kontinent Afrika berichten und uns seine Vielfalt mit ihren Büchern näherbringen.
Wir freuen uns sehr, diese Veranstaltung mit einem Büchertisch begleiten zu dürfen. Neben den Büchern der Autorin werden wir zu diesem Anlass Literatur rund um den Schwerpunkt "Afrika" präsentieren.
Nähere Informationen zur Veranstaltung unter
**www.katholische-akademie-freiburg.de**

----

Timestart: 16:00:00

----

Timeend: 17:00:00

----

Dateend: 2019-04-30

----

Showtime: true

----

Multipledays: false

----

Draft: 1
