Title: HerbstLESE im Fundevogel

----

Location: Buchhandlung Fundevogel

----

Date: 2016-11-12

----

Link:

----

Category: Präsentation und Vorstellung ausgewählter Neuerscheinungen

----

Text:

Dieser Abend besitzt im Fundevogel bereits Tradition und ist ganz den Neuerscheinungen des Bücherherbstes aus dem Bereich der Kinder- und Jugendliteratur gewidmet.

Aus der Vielzahl der Neuheiten haben wir für Euch eine Auswahl von empfehlenswerten Titeln getroffen, um sie Euch in gemütlicher Atmosphäre bei Sekt und Selters, Wein, Saft und Brezeln persönlich vorzustellen. So möchten wir Euch einen individuell geprägten Überblick über die Neuheiten dieses Herbstes 2016 geben. Lasst Euch von uns in die Welt der Bücher entführen.

**Ihr seid herzlich eingeladen!**

*Der Eintritt ist frei. Wir bitten um vorherige Anmeldung!*

----

Timestart: 19:00:00

----

Timeend:

----

Dateend: 2016-11-12

----

Showtime: true

----

Multipledays: false

----

Draft: 1
